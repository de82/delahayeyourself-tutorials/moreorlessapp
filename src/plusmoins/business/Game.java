package plusmoins.business;

import java.util.Random;

/**
 *
 * @author sam
 */
public class Game {
    
    public static int LESS = 12;
    public static int MORE = 13;
    public static int EQUAL = 14;
    
    private int numberToGuess;
    private int totalTry;
    
    public Game(){
        this.reset();
    }
    
    public int guess(int number){
        this.totalTry += 1;
        if(this.numberToGuess == number){
            return Game.EQUAL;
        }else if(this.numberToGuess > number){
            return Game.MORE;
        }else{
            return Game.LESS;
        }
    }
    
    public void reset(){
        this.totalTry = 0;
        this.numberToGuess = this.randomNumber(1, 3);
    }
    
    public int randomNumber(int min, int max){
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min; 
    }

    public int getNumberToGuess() {
        return numberToGuess;
    }

    public int getTotalTry() {
        return totalTry;
    }
    
}
